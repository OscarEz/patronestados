/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PEstados;

/**
 *
 * @author cesarflores
 */
public class Alarma {
    	private Estado miEstado; //atributo de tipo estado

	public void setEstado(Estado e) //metodo que recibe un objeto de tipo estado
	{
	  this.miEstado = e;   //asigna al atributo estado.
	}
	public void ejecutarAccion()
	{
		miEstado.ejecutarAccion();
	}
}
